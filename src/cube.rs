use bevy::pbr::CascadeShadowConfigBuilder;
use bevy::prelude::{shape::Plane, *};
use std::f32::consts::{FRAC_PI_2, FRAC_PI_3, FRAC_PI_4, PI};
pub struct CubePlugin;

#[derive(Component)]
pub struct Cube {
    pub(crate) sides: [Side; 6],
}

pub struct Side {
    pub(crate) tiles: [[Entity; TILES_PER_SIDE]; TILES_PER_SIDE],
}

#[derive(Component)]
pub struct Tile {
    x: usize,
    y: usize,
}

pub const TILES_PER_SIDE: usize = 10;
pub const CUBE_SIDE_SIZE: f32 = 128.;
pub const CUBE_TILE_GAP_SIZE: f32 = 1.;
pub const TILE_Z_HEIGHT: f32 = 1.;
pub const FACE_Z_HEIGHT: f32 = 1.;
pub const TILE_SIZE: f32 =
    (CUBE_SIDE_SIZE - (1 + TILES_PER_SIDE) as f32 * CUBE_TILE_GAP_SIZE) / TILES_PER_SIDE as f32;

impl Plugin for CubePlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(spawn_cube);
        app.add_system(rotate_cube);
        // app.add_system(flap_tiles);
    }
}

pub(crate) fn spawn_cube(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    let cube_material = materials.add(StandardMaterial {
        base_color: Color::rgb(0.75, 0.4, 0.5),
        perceptual_roughness: 0.7,
        metallic: 0.1,
        double_sided: false,
        ..default()
    });

    let face_material = materials.add(StandardMaterial {
        base_color: Color::rgb(0.5, 0.8, 0.7),
        perceptual_roughness: 0.7,
        metallic: 0.1,
        double_sided: false,
        ..default()
    });

    let tile_material = materials.add(StandardMaterial {
        base_color: Color::rgb(0.8, 0.8, 0.8),
        perceptual_roughness: 0.3,
        metallic: 0.5,
        double_sided: false,
        ..default()
    });

    let tile_top_left_start = -CUBE_SIDE_SIZE * 0.5 + CUBE_TILE_GAP_SIZE + TILE_SIZE / 2.;
    let tile_mesh = meshes.add(Mesh::from(shape::Box::new(
        TILE_SIZE,
        TILE_SIZE,
        TILE_Z_HEIGHT,
    ))); // todo: real model?

    // shape::Plane doesn't seem to display and I don't know why!
    let face = meshes.add(Mesh::from(shape::Box::new(
        CUBE_SIDE_SIZE,
        CUBE_SIDE_SIZE,
        FACE_Z_HEIGHT,
    )));

    /// spawn sides
    let cube = meshes.add(Mesh::from(shape::Cube::new(CUBE_SIDE_SIZE)));

    let mut cube = commands.spawn((
        Transform::default(),
        GlobalTransform::default(),
        Visibility::default(),
        ComputedVisibility::default(),
    ));

    // This rotates sides of the cube to look like a cube. Some may be upside down?
    let face_transforms = [
        Transform {
            translation: Vec3::Z * (CUBE_SIDE_SIZE + FACE_Z_HEIGHT) * -0.5,
            ..default()
        },
        // Transform::default(),
        Transform::from_rotation(Quat::from_rotation_y(PI))
            * Transform {
                translation: Vec3::Z * (CUBE_SIDE_SIZE + FACE_Z_HEIGHT) * -0.5,
                ..default()
            },
        Transform::from_rotation(Quat::from_rotation_y(FRAC_PI_2))
            * Transform {
                translation: Vec3::Z * (CUBE_SIDE_SIZE + FACE_Z_HEIGHT) * -0.5,
                ..default()
            },
        Transform::from_rotation(Quat::from_rotation_y(PI + FRAC_PI_2))
            * Transform {
                translation: Vec3::Z * (CUBE_SIDE_SIZE + FACE_Z_HEIGHT) * -0.5,
                ..default()
            },
        Transform::from_rotation(Quat::from_rotation_x(PI + FRAC_PI_2))
            * Transform {
                translation: Vec3::Z * (CUBE_SIDE_SIZE + FACE_Z_HEIGHT) * -0.5,
                ..default()
            },
        Transform::from_rotation(Quat::from_rotation_x(FRAC_PI_2))
            * Transform {
                translation: Vec3::Z * (CUBE_SIDE_SIZE + FACE_Z_HEIGHT) * -0.5,
                ..default()
            },
    ];

    let cube_entity = cube.id();
    let sides = std::array::from_fn(|i| {
        // side has easy access to its children tiles by x,y
        let mut tiles = [[Entity::from_raw(0); TILES_PER_SIDE]; TILES_PER_SIDE];
        let mut side = commands
            .spawn((PbrBundle {
                mesh: face.clone(),
                material: face_material.clone(),
                transform: face_transforms[i],
                ..default()
            },))
            .set_parent(cube_entity)
            .with_children(|commands| {
                // tiles are children of the side, so that they inherit side's transform
                for y in 0..TILES_PER_SIDE {
                    for x in 0..TILES_PER_SIDE {
                        let tile = commands.spawn((
                            Tile { x, y },
                            PbrBundle {
                                mesh: tile_mesh.clone(),
                                material: if x == 0 && y == 0 {
                                    face_material.clone()
                                } else {
                                    tile_material.clone()
                                },
                                transform: Transform {
                                    translation: Vec3::new(
                                        (TILE_SIZE + CUBE_TILE_GAP_SIZE) * x as f32
                                            + tile_top_left_start,
                                        tile_top_left_start
                                            + (TILE_SIZE + CUBE_TILE_GAP_SIZE) * y as f32,
                                        (TILE_Z_HEIGHT + FACE_Z_HEIGHT) * -0.5,
                                    ),
                                    ..default()
                                },
                                ..default()
                            },
                        ));
                        tiles[y][x] = tile.id();
                    }
                }
            });
        Side { tiles }
    });
    commands.entity(cube_entity).insert(Cube { sides });

    // background
    let bg_material = materials.add(StandardMaterial {
        base_color: Color::rgb(0.3, 0.4, 0.8),
        perceptual_roughness: 1.,
        metallic: 0.,
        double_sided: false,
        unlit: true,
        ..default()
    });
    // TODO this could be a skybox?
    let background = meshes.add(Mesh::from(shape::Plane::from_size(100000.)));

    commands.spawn((PbrBundle {
        mesh: background,
        material: bg_material,
        transform: Transform {
            translation: Vec3::new(0., 0., 10000.),
            rotation: Quat::from_rotation_x(-FRAC_PI_2),
            ..default()
        },
        ..default()
    },));

    // Two dimmer lights with an offset could add a bit of anti-aliasing to shadows
    commands.spawn(
        (DirectionalLightBundle {
            directional_light: DirectionalLight {
                illuminance: 35000.,
                shadows_enabled: true,
                ..default()
            },
            transform: Transform {
                rotation: Quat::from_xyzw(-FRAC_PI_3, -PI / 12., 0., 1.),
                ..default()
            },
            cascade_shadow_config: CascadeShadowConfigBuilder {
                first_cascade_far_bound: CUBE_SIDE_SIZE,
                ..default()
            }
            .build(),
            ..default()
        }),
    );
}

/// an effect
// fn flap_tiles(time: Res<Time>, mut tiles: Query<(&mut Transform, &Tile)>) {
//     let delta = time.elapsed_seconds_wrapped();
//     for (mut t, tile) in tiles.iter_mut() {
//         t.translation.z = (TILE_Z_HEIGHT + FACE_Z_HEIGHT) * -0.5 +
//             ((delta + (0.6 * tile.x as f32) + (0.5 * tile.y as f32)).sin() * TILE_Z_HEIGHT + TILE_Z_HEIGHT) * 0.25;
//     }
// }

fn rotate_cube(time: Res<Time>, mut cubes: Query<&mut Transform, With<Cube>>) {
    let delta = time.delta_seconds() * 0.014;
    for mut t in cubes.iter_mut() {
        t.rotation *= Quat::from_rotation_y(delta)
            * Quat::from_rotation_x(delta * 0.3)
            * Quat::from_rotation_z(delta * 0.1);
    }
}
