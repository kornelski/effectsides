use crate::cube::{spawn_cube, Cube, TILES_PER_SIDE, TILE_SIZE, TILE_Z_HEIGHT};
use bevy::prelude::{shape::UVSphere, *};
use std::f32::consts::{FRAC_PI_2, FRAC_PI_4, PI};
pub struct PlayerPlugin;

#[derive(Component)]
pub struct Player {
    side: usize,
    tile_x: usize,
    tile_y: usize,
    facing: u8, // 0-3
}

#[derive(Debug, Clone, Copy)]
pub enum PlayerAction {
    Spawn {
        side: usize,
        tile_x: usize,
        tile_y: usize,
    },
    MoveForward,
    MoveBackward,
    TurnLeft,
    TurnRight,
}

const PLAYER_BODY_RADIUS: f32 = TILE_SIZE * 0.4;

impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<PlayerAction>();

        app.add_startup_system(spawn_player);
        app.add_system(player_events);
        app.add_system(player_anim);
    }
}

// spawn is an event, because it needs the cube to exist before player may be placed, and it will be needed later for respawning anyway
fn spawn_player(mut events: EventWriter<PlayerAction>) {
    events.send(PlayerAction::Spawn {
        side: 0,
        tile_x: 3,
        tile_y: 2,
    });
}

fn player_events(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    mut events: EventReader<PlayerAction>,
    cube: Query<(&Cube)>,
    mut player: Query<(Entity, &mut Player, &mut Transform)>,
) {
    // if the cube is not there, the game hasn't started yet
    let Ok(cube) = cube.get_single() else { return };

    for event in &mut events {
        match event {
            // these are sent in response to key events in game.rs
            // TODO: there should be some throttling here
            PlayerAction::MoveForward => {
                update_player_position(&mut player, cube, &mut commands, 1, 0);
            }
            PlayerAction::MoveBackward => {
                update_player_position(&mut player, cube, &mut commands, -1, 0);
            }
            PlayerAction::TurnLeft => {
                update_player_position(&mut player, cube, &mut commands, 0, -1);
            }
            PlayerAction::TurnRight => {
                update_player_position(&mut player, cube, &mut commands, 0, 1);
            }
            // makes the player entity
            &PlayerAction::Spawn {
                side,
                tile_x,
                tile_y,
            } => {
                let player_material = materials.add(StandardMaterial {
                    base_color: Color::rgb(0.75, 0.2, 0.9),
                    perceptual_roughness: 0.5,
                    metallic: 0.7,
                    double_sided: false,
                    ..default()
                });

                let player_mesh = meshes.add(Mesh::from(UVSphere {
                    radius: PLAYER_BODY_RADIUS,
                    ..default()
                }));

                let tile = cube.sides[side].tiles[tile_x][tile_y];
                let facing = 0;
                commands.entity(tile).with_children(|commands| {
                    commands
                        .spawn((
                            Player {
                                side,
                                tile_x,
                                tile_y,
                                facing,
                            },
                            PbrBundle {
                                mesh: player_mesh.clone(),
                                material: player_material.clone(),
                                transform: Transform {
                                    translation: Vec3::Z * -PLAYER_BODY_RADIUS,
                                    rotation: Quat::from_rotation_z(
                                        ((facing + 1) as f32) * FRAC_PI_2,
                                    ),
                                    ..default()
                                },
                                ..default()
                            },
                        ))
                        .with_children(|commands| {
                            // hands
                            commands.spawn(PbrBundle {
                                mesh: player_mesh.clone(),
                                material: player_material.clone(),
                                transform: Transform {
                                    translation: Vec3::X * -PLAYER_BODY_RADIUS,
                                    rotation: Quat::from_rotation_x(FRAC_PI_2),
                                    scale: Vec3::new(0.3, 0.3, 0.3),
                                    ..default()
                                },
                                ..default()
                            });
                            commands.spawn(PbrBundle {
                                mesh: player_mesh.clone(),
                                material: player_material.clone(),
                                transform: Transform {
                                    translation: Vec3::X * PLAYER_BODY_RADIUS,
                                    rotation: Quat::from_rotation_x(FRAC_PI_2),
                                    scale: Vec3::new(0.3, 0.3, 0.3),
                                    ..default()
                                },
                                ..default()
                            });
                        });
                });
            }
        }
    }
}

fn update_player_position(
    player: &mut Query<(Entity, &mut Player, &mut Transform)>,
    cube: &Cube,
    commands: &mut Commands,
    fwd: i8,
    rotation: i8,
) {
    let (mut e, mut player, mut t) = player.single_mut();
    player.facing = player.facing.wrapping_add_signed(rotation) & 3;
    let (x, y) = match player.facing {
        0 => (0, 1),
        1 => (1, 0),
        2 => (0, -1),
        _ => (-1, 0),
    };
    player.tile_y = player
        .tile_y
        .saturating_add_signed(y * fwd as isize) // TODO: this should move to another side, also should check the "map" which doesn't exist yet
        .min(TILES_PER_SIDE - 1);
    player.tile_x = player
        .tile_x
        .saturating_add_signed(x * fwd as isize)
        .min(TILES_PER_SIDE - 1);

    // TODO: rotation
    let tile = cube.sides[player.side].tiles[player.tile_x][player.tile_y];

    t.rotation = Quat::from_rotation_z(((player.facing + 1) as f32) * FRAC_PI_2);

    // this is a lazy way to update the position. Being child of the tile it automatically gets
    // the right orientation,
    // TODO: after move it'd be nice to animate from the previous tile. That needs transforms magic and some tweening.
    commands.entity(e).set_parent(tile);
}

// that's just a nonsense jumpy stance to make the blob feel alive
fn player_anim(time: Res<Time>, mut player: Query<&mut Transform, With<Player>>) {
    let time = time.elapsed_seconds_wrapped();
    for mut t in player.iter_mut() {
        t.scale = Vec3::new(1., 1., 0.9 + ((time * 7.).sin() + 2.) * 0.05);
        t.translation = (Vec3::Z * -PLAYER_BODY_RADIUS);
        t.translation.z += (time * 7.).cos() * PLAYER_BODY_RADIUS * 0.1;
    }
}
