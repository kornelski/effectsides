#![cfg_attr(debug_assertions, allow(unused))] // shush Rust about unfinished code

use bevy::prelude::*;
mod camera;
mod cube;
mod game;
mod player;

fn main() {
    App::new()
        .add_plugins(DefaultPlugins) // Bevy
        .add_plugin(game::GamePlugin) // Startup
        .add_plugin(cube::CubePlugin)
        .add_plugin(camera::CameraPlugin)
        .add_plugin(player::PlayerPlugin)
        .run();
}
