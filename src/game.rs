use bevy::prelude::*;
use std::time::Duration;

use crate::player::PlayerAction;

pub struct GamePlugin;

impl Plugin for GamePlugin {
    fn build(&self, app: &mut App) {
        app.add_system(slowdown);

        app.add_system(read_input);
    }
}

fn slowdown() {
    #[cfg(all(debug_assertions, target_os = "macos"))]
    std::thread::sleep(Duration::from_millis(15)); // bevy on macos flickers ;( https://github.com/bevyengine/bevy/issues/3585
}

// arrows or WASD
fn read_input(keyboard_input: Res<Input<KeyCode>>, mut player: EventWriter<PlayerAction>) {
    if keyboard_input.just_pressed(KeyCode::W) || keyboard_input.just_pressed(KeyCode::Up) {
        player.send(PlayerAction::MoveForward);
    }
    if keyboard_input.just_pressed(KeyCode::S) || keyboard_input.just_pressed(KeyCode::Down) {
        player.send(PlayerAction::MoveBackward);
    }
    if keyboard_input.just_pressed(KeyCode::A) || keyboard_input.just_pressed(KeyCode::Left) {
        player.send(PlayerAction::TurnLeft);
    }
    if keyboard_input.just_pressed(KeyCode::D) || keyboard_input.just_pressed(KeyCode::Right) {
        player.send(PlayerAction::TurnRight);
    }
}
