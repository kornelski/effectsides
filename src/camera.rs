use bevy::prelude::*;

// this could be replaced with a proper camera plugin or some dynamic following, once there's something to follow

pub struct CameraPlugin;

impl Plugin for CameraPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(spawn_camera);
    }
}

fn spawn_camera(mut commands: Commands) {
    commands.spawn(Camera3dBundle {
        transform: Transform::from_translation(Vec3::new(0., -100., -300.))
            .looking_at(Vec3::ZERO, Vec3::Y),
        ..default()
    });
}
